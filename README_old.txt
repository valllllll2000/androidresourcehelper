=====AndroidRessourceHelper====

==Objectifs==
Ce programme facilite la traductions des ressources textes d'une application Android.
Tous les textes visibles à l'utilisateur doivent être inscrits dans des fichiers XML.
Il y aura autant de dossiers values-[locale] contenant des fichiers strings.xml que
de langues vers lesquelles l'on souhaite traduire notre application Android. le dossier 'values'
contient les ressources dans la langue par défaut. Le programme permettra également la sauvegarde
des traductions dans un fichier EXCEL pour faciliter la traduction. On pourra ensuite extraire
toutes les traductions et créer plusieurs fichiers strings.xml en fonction des langues disponibles.
Le programme permettra une traduction automatique en plusieurs langues. Cette traduction facilitera la tâche du
traducteur qui n'aura plus qu'à corriger les erreurs de traduction.

==Librairies utilisées==
JXL: https://sourceforge.net/projects/jexcelapi/?source=dlp
Cette librairie permet la lecture et l’écriture des fichiers EXCEL.

Microsoft Translation
http://www.microsofttranslator.com/dev/
Outil de traduction automatique gratuit mais limité à 2 millions de caractères para mois.

JNLP
http://docs.oracle.com/javase/tutorial/deployment/deploymentInDepth/jnlp.html
permet l'ouverture de fichiers

microsoft-translator-java-api-0.6.2-jar-with-dependencies.jar
https://github.com/boatmeme/microsoft-translator-java-api
Permet l'utilisation du service de Microsoft en JAVA.


==Utilisation==
Une fois le programme importé dans un IDE et lancé, une fenetre apparaitra et on aura le choix entre plusieurs
options pour tester le programme.
L'utilisateur aura le choix entre:

1. Extraire un fichier ressource et écrire les données dans un fichier EXCEL

2. Lire un fichier EXCEL et créer le fichier XML correspondant

3. Extraire les ressources  a partir d'un dossier 'res' contenant plusieurs traductions

4. Ouvrir soit un fichier XML, soit EXCEL

5. Effectuer une traduction complete en 6 langues à partir d'un fichier dans une des langues suivantes:
anglais, français, russe, allemand, espagnol, catalan, portugais
Le programme crééra les six dossiers, un pour chaque langue et y insèrera le fichier strings.xml avec les
traductions effectuées avec l'outil Microsoft Translator.


==Outils nécessaires==
Le programme doit pouvoir être utilisé sous Linux mais aussi sous Windows et Mac mais il n'a été testé
qu'avec Ubuntu 13.04 64 bits.
Il faut installer le SDK 1.7 ou 1.6 et éventuellement GIT. http://git-scm.com/
Pour le développement j'ai utilisé IntelliJ IDEA mais le projet peut également être importé sous eclipse.
Le projet peut être exécuté directement avec IntelliJ.
http://www.jetbrains.com/idea/
Il est recommandé d'avoir des fichiers de ressources pour tester (fournis).
Pour tester la traduction automatique, il faut une connection internet.

==Pour aller plus loin==
L'interface grahique pourrait être revue et améliorée.
Lorsque l'on effectue la traduction automatique, cela prend du temps, hors aucun message d'attente n'est montré
à l'utilisateur. Ce genre de tâche devrait être implementée dans un autre fil d'exécution (Thread), je prevois
de le faire plus tard.
Il serait aussi intéressant d'offrir à l'utilisateur la possibilité de choisir le fichier à traiter et ensuite
déduire ce qu'il veut faire.

