=====AndroidRessourceHelper====

This library allows you to extract Android string resources, store them in Excel file, export Android resources to an
Excel file and automatic translation

In order to use the automatic translation feature you need to register here and get yourself a client id and client
secret:
http://msdn.microsoft.com/en-us/library/hh454950.aspx

Then you need to update config.properties file to replace the default values by yours