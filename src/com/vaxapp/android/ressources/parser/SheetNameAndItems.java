package com.vaxapp.android.ressources.parser;

import java.util.ArrayList;

/**
 * Created by valeria on 10/8/14.
 */
public class SheetNameAndItems {
    protected final String sheetName;
    protected final ArrayList<XmlItem> elements;

    public SheetNameAndItems(String sheetName, ArrayList<XmlItem> elements) {
        this.sheetName = sheetName;
        this.elements = elements;
    }
}
