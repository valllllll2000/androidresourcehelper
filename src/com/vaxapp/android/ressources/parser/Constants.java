package com.vaxapp.android.ressources.parser;

public class Constants {

    public static final String STRING_VALUES = "string";
    public static final String ARRAY_VALUES = "string-array";
    public static final String NAME_ATTRIBUTE = "name";
    public static final String ITEM_FIELD = "item";

}
