package com.vaxapp.android.ressources.parser;

import com.memetix.mst.language.Language;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: valeria
 * Date: 9/15/13
 * Time: 9:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileHelper {

    public static final String LINUX_TEXT_EDITOR = "gedit";
    public static final String DEFAULT_XML_FILE_NAME = "strings.xml";
    public static final String DATA_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT = "dd-MM-yyyy_HH:mm:ss";
    public static final String DEFAULT_SHEETNAME = "default";
    public static final String VALUES_FOLDER = "values";
    public static final String TEST_FOLDER_RES_VALUES_STRINGS_XML = "test_folder/res/values/strings.xml";

    /***
     * Lit une chaine avec le nom du fichier ou chemin
     * @return
     */
    public static String readFileName() {
        String nom = null;
        try {
            Scanner scanner = new Scanner(System.in);
            nom = scanner.next();
        } catch (InputMismatchException e) {
            e.printStackTrace();
        }
        return nom;
    }

    /**
     * Lit un fichier xml et en extrait les donnees, puis ecrit les
     * données dans un ficher excel
     * @param originXMLFileName
     * @param targetXLFilePath
     */
    public static void createXL(String originXMLFileName, String targetXLFilePath) {
        try {
            Tools.logMessage("xl file name : " + targetXLFilePath);
            ExcelReadWrite writer = new ExcelReadWrite(targetXLFilePath);
            writer.writeExcel(XmlReadWrite.getXmlItems(originXMLFileName), DEFAULT_SHEETNAME, 0);
            Tools.logMessage("Fichier excel crée " + writer.getFileName());
        } catch (Exception e) {
            Tools.logErrorMessage("Erreur de création du fichier xl");
            e.printStackTrace();
        }
    }

    /**
     * Lit les fichiers a traiter sous format xml,
     * extrait les donnees puis ecrit les
     * données dans un ficher excel
     * s'il y a plusieurs langues dans le meme dossier,
     * on ajoutera des colonnes supplémentaires
     * @param xmlResourcesFolder
     */
    public static void createXLFromFolder(String xmlResourcesFolder) {
        ArrayList<String> files = getXMLFiles(xmlResourcesFolder);
        if(files!=null && files.size()>0)  {
            ExcelReadWrite writer = new ExcelReadWrite(getDefaultXlFileName());
            XmlReadWrite reader = new XmlReadWrite();
            int sheetNumber = 0;
            for(String fileName : files){
                ArrayList<XmlItem> elements = reader.parseDocument(fileName + File.separator + DEFAULT_XML_FILE_NAME);
                writeToExcelSheet(writer, sheetNumber, fileName, elements);
                sheetNumber++;
            }
            Tools.logMessage("Fichier excel crée " + writer.getFileName());
        } else {
            MainClass.informExit("Aucun fichier trouvé dans " + xmlResourcesFolder);
        }
    }

    private static void writeToExcelSheet(ExcelReadWrite writer, int sheetNumber, String fileName, ArrayList<XmlItem> elements) {
        String [] temp = fileName.split(File.separator);
        String fileSimpleName = temp == null || temp.length == 0 ? null : temp[temp.length - 1];
        String sheetName = fileSimpleName == null ? DEFAULT_SHEETNAME : fileSimpleName;
        writer.writeExcel(elements, sheetName, sheetNumber);
    }

    static String getDefaultXlFileName() {
        return "excel_test"+getFormattedDate(DATE_FORMAT)+".xls";
    }

    public static String getFormattedDate(String dataFormat) {
        return new SimpleDateFormat(dataFormat, Locale.getDefault())
                .format(new Date());
    }

    /***
     * Renvoie la liste des fichiers XML différents,
     *  dans un dossier donnés
     * @param folderToUse
     * @return
     */
    private static ArrayList<String> getXMLFiles(String folderToUse) {
        ArrayList<String> files = new ArrayList<String>();
        File[] allFiles = new File(folderToUse).listFiles();
        if(allFiles!=null)
            for (File f : allFiles) {
                Tools.logMessage("file=" + f.getAbsolutePath());
                if (f.getName().startsWith(VALUES_FOLDER)) {
                    Tools.logMessage("filename=" + f.getName());
                    files.add(f.getAbsolutePath());
                }
            }
        return files;
    }

    /***
     * Lit un fichier excel extrait les chaines et leur traductions crée le
     * fichier strings.
     * @param filePath
     *
     */
    public static void createXML(String filePath) {
        ExcelReadWrite reader = new ExcelReadWrite(filePath);
        ArrayList<XmlItem> items = new ArrayList<XmlItem>();
        try {
            int sheets = reader.getNumberOfSheets();
            String prefix_folder = "res" + "_" + getFormattedDate(DATE_FORMAT);
            for (int i = 0; i < sheets; i++) {
                SheetNameAndItems sheetNameAndItems = reader.readExcel(i);
                items = sheetNameAndItems.elements;
                XmlReadWrite writer = new XmlReadWrite();
                writer.writeXmlFile(prefix_folder + File.separator + sheetNameAndItems.sheetName,
                        DEFAULT_XML_FILE_NAME, items);
                Tools.logMessage("Fichier xml crée " + writer.getFileName());
            }

        }   catch (Exception e){
            e.printStackTrace();
            MainClass.informExit("Erreur de creation de fichier " + DEFAULT_XML_FILE_NAME);
        }
    }

    /***
     * ouverture du fichier :
     * @param path
     */
    static void openFile(String path) {
        Tools.logMessage("Ouverture du fichier: " + path);
        try {
            Tools.OsType osType = Tools.getOsType();
            if (osType == Tools.OsType.LINUX) {
                Tools.FileType fileType = Tools.getFileType(path);
                if(fileType == Tools.FileType.XML) {
                    Runtime.getRuntime().exec(new String[]{LINUX_TEXT_EDITOR, path});
                } else if(fileType==Tools.FileType.CALC) {
                    openXlFile(path);
                }  else {
                    Tools.logErrorMessage("Extension inconnue");
                }
            } else {
                Desktop.getDesktop().open(new File(path));
            }
        } catch (Exception e) {
            Tools.logErrorMessage("Erreur d'ouverture du fichier : " + path);
            e.printStackTrace();
        }
    }

    private static void openXlFile(String path) throws IOException {
        try {
            Runtime.getRuntime().exec(new String[]{"localc",path});
        } catch (Exception e) {
            Runtime.getRuntime().exec(new String[]{"oocalc",path});
        }
    }

    public static void writeToExcelFile(ArrayList<SheetNameAndItems> sheetNameAndItemses) {
        ExcelReadWrite writer = new ExcelReadWrite(FileHelper.getDefaultXlFileName());
        int sheetNumber = 0;
        for (SheetNameAndItems item : sheetNameAndItemses) {
            FileHelper.writeToExcelSheet(writer, sheetNumber, item.sheetName, item.elements);
            sheetNumber++;
        }
    }
}
