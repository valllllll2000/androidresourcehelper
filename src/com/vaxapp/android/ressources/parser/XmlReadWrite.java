package com.vaxapp.android.ressources.parser;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.memetix.mst.language.Language;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/***
 * Extract string and string-array values from an xml file
 *
 * Writes content to xml files
 *
 */
public class XmlReadWrite {

    private static final String ROOT_ELEMENT = "ressources";
    private String filePath;

    public static ArrayList<XmlItem> getXmlItems(String originXMLFileName) {
        ArrayList<XmlItem> items;
        Tools.logMessage("xml file : " + originXMLFileName);
        items = parseDocument(originXMLFileName);
        return items;
    }

    public String getFileName() {
        return filePath;
    }

    public static ArrayList<XmlItem> parseDocument(String docName) {
        ArrayList<XmlItem> textes = new ArrayList<XmlItem>();

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder builder = null;
        try {
            builder = builderFactory.newDocumentBuilder();
            Document document = builder.parse(new FileInputStream(docName));
            document.getDocumentElement().normalize();

            // Element rootElement = document.getDocumentElement();
            // Tools.logMessage("Root element :" + rootElement.getNodeName());
            NodeList stringArrays = document
                    .getElementsByTagName(Constants.ARRAY_VALUES);
            NodeList strings = document
                    .getElementsByTagName(Constants.STRING_VALUES);
            textes.addAll(getStrings(strings));
            textes.addAll(getArrays(stringArrays));

        }

        catch (Exception e) {
            e.printStackTrace();
        }
        return textes;

    }

    /***
     * Etrait les elements <string/>
     *
     * @param strings
     *            : liste de nodes
     * @return
     */
    private static ArrayList<XmlItem> getStrings(NodeList strings) {
        ArrayList<XmlItem> xmlItems = new ArrayList<XmlItem>();
        for (int temp = 0; temp < strings.getLength(); temp++) {

            Node nNode = strings.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String name = eElement.getAttribute(Constants.NAME_ATTRIBUTE);
//                Tools.logMessage("name is : " + name);

                XmlItem item = new XmlItem();
                item.setItemType(XmlItem.Type.STRING);
                item.setName(name);
                item.setStringValue(eElement.getTextContent());
                xmlItems.add(item);

            }
        }
        return xmlItems;
    }

    /***
     * Etrait les elements <string/>
     *
     * @param stringArrays
     *            : liste de nodes
     * @return
     */
    private static ArrayList<XmlItem> getArrays(NodeList stringArrays) {
        ArrayList<XmlItem> xmlItems = new ArrayList<XmlItem>();
        for (int temp = 0; temp < stringArrays.getLength(); temp++) {

            Node arrayNode = stringArrays.item(temp);
//            Tools.logMessage("\nCurrent Element :" + arrayNode.getNodeName());

            if (arrayNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) arrayNode;
                String name = eElement.getAttribute(Constants.NAME_ATTRIBUTE);
//                Tools.logMessage("name is : " + name);

                XmlItem item = new XmlItem();
                item.setItemType(XmlItem.Type.ARRAY);
                item.setName(name);
                // item.setStringValue(eElement.getTextContent());
                NodeList its = eElement.getChildNodes();
//                Tools.logMessage("items nb=" + its.getLength());
                ArrayList<String> arrs = new ArrayList<String>();
                for (int k = 0; k < its.getLength(); k++) {
                    Node itemNode = its.item(k);
                    if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element itemElement = (Element) itemNode;
                        arrs.add(itemElement.getTextContent());
                    }

                }
                item.setArrayValue(arrs);

                xmlItems.add(item);

            }
        }
        return xmlItems;
    }

    private ArrayList<XmlItem> getItemsFromNode(NodeList arrayChildren,
                                                XmlItem.Type type, String lang) {

        ArrayList<XmlItem> xmlItems = new ArrayList<XmlItem>();

        for (int i = 0; i < arrayChildren.getLength(); i++) {

            Node node1 = arrayChildren.item(i);
            if (node1.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) node1;
                XmlItem item = new XmlItem();
                item.setItemType(type);
                String value = eElement.getAttribute(Constants.NAME_ATTRIBUTE);

                item.setName(value);
                Tools.logMessage("Item name is " + item.getName());
                item.setLang(lang);

                if (type == XmlItem.Type.ARRAY) {
                    NodeList arrayItems = node1.getChildNodes();
                    int nbItems = arrayItems.getLength();
                    ArrayList<String> itemValues = new ArrayList<String>(
                            nbItems);
                    for (int j = 0; j < nbItems; j++) {
                        Node itemNode = arrayItems.item(i);
                        if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
                            itemValues.add(itemNode.getTextContent());
                        }
                    }
                    item.setArrayValue(itemValues);
                } else {
                    item.setStringValue(node1.getTextContent());
                }

                xmlItems.add(item);
            }
        }
        return xmlItems;
    }

    /**
     *
     * @param folderName
     *            : may be null
     * @param fileName
     * @param elements
     */
    public void writeXmlFile(String folderName, String fileName,
                             ArrayList<XmlItem> elements) {
        try {
            File file = getXmlFile(folderName, fileName);
            filePath = file.getAbsolutePath();
            parseAndWriteItemsToFile(elements, file);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void parseAndWriteItemsToFile(ArrayList<XmlItem> elements, File file)
    {
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement(ROOT_ELEMENT);
            doc.appendChild(rootElement);

            for (XmlItem it : elements) {
                Attr attr = doc.createAttribute(Constants.NAME_ATTRIBUTE);
                attr.setValue(it.getName());

                if (it.getItemType() == XmlItem.Type.ARRAY) {
                    Element stringArray = doc
                            .createElement(Constants.ARRAY_VALUES);

                    stringArray.setAttributeNode(attr);
                    ArrayList<String> values = it.getArrayValue();

                    for (String s : values) {
                        Element item = doc.createElement(Constants.ITEM_FIELD);
                        item.appendChild(doc.createTextNode(s));
                        stringArray.appendChild(item);
                        rootElement.appendChild(stringArray);
                    }

                } else {
                    Element string = doc.createElement(Constants.STRING_VALUES);

                    string.setAttributeNode(attr);
                    string.appendChild(doc.createTextNode(it.getStringValue()));
                    rootElement.appendChild(string);

                }
            }

            Transformer transformer = TransformerFactory.newInstance()
                    .newTransformer();
            DOMSource source = new DOMSource(doc);

            StreamResult result = new StreamResult(file);

            transformer.transform(source, result);

            Tools.logMessage("Data written successfully to " + file.getAbsolutePath());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private static File getXmlFile(String folderName, String fileName) {
        File file;
        if (folderName != null) {
            File folder = new File(folderName);
            if (!folder.exists())
                folder.mkdirs();
            file = new File(folder, fileName);
        } else {
            file = new File(fileName);
        }
        return file;
    }

    public static void writeLanguageToXmlFile(String originXMLFileName,
                                              Language languageTo, ArrayList<XmlItem> newItems) {

        File toWrite = getFileNameForLanguage(originXMLFileName, languageTo);
        parseAndWriteItemsToFile(newItems, toWrite);
    }

    private static File getFileNameForLanguage(String originXMLFileName, Language language) {
        String fileName =  FileHelper.DEFAULT_XML_FILE_NAME;
        originXMLFileName = originXMLFileName.replace(fileName,"");
        String valuesString = FileHelper.VALUES_FOLDER;
        String defaultFolderForLanguage = valuesString +"-"+language.toString();
        Tools.logMessage("defaultFolderForLanguage : "+defaultFolderForLanguage);
        String [] tempFileNames = originXMLFileName.split(File.separator);
        boolean addFolder = false;
        if(tempFileNames[tempFileNames.length-1].contains(valuesString)) {
            tempFileNames[tempFileNames.length-1] = defaultFolderForLanguage;
        } else {
            addFolder = true ;
        }

        StringBuffer folderFullPath = new StringBuffer();

        for(String s : tempFileNames) {
            folderFullPath.append(s);
            folderFullPath.append(File.separator);
        }
        folderFullPath = folderFullPath.deleteCharAt(folderFullPath.length()-1);
        if(addFolder){
            folderFullPath.append(File.separator);
            folderFullPath.append(defaultFolderForLanguage);
        }
        Tools.logMessage(folderFullPath.toString());
        File file = getXmlFile(folderFullPath.toString(), fileName);
        Tools.logMessage("Xml file full path : "+file.getAbsolutePath());
        return file;
    }
}
