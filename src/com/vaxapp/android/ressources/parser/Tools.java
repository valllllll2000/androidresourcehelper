package com.vaxapp.android.ressources.parser;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: valeria
 * Date: 9/15/13
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class Tools {

    public static final String[] XL_EXTENSIONS = { "xl", "xls", "ods" };
    public static final String[] XML_EXTENSIONS = { "xml" };
    public static final String FILE_EXTENSION_DOT = "\\.";

    public static FileType getFileType(String path) {
        if(isCalcFile(path)) {
            return FileType.CALC;
        } else if(isXmlFile(path)) {
            return FileType.XML;
        }
        return FileType.OTHER;
    }

    private static boolean isXmlFile(String path) {
        return isExpectedFileExtension(path,XML_EXTENSIONS);
    }

    private static boolean isCalcFile(String path) {
        return isExpectedFileExtension(path,XL_EXTENSIONS);
    }

    public static void logMessage(String message) {
        System.out.println(message);
    }

    public static void logErrorMessage(String errorMessage) {
        System.err.println(errorMessage);
    }

    public static String getFirstNonEmptyString(ArrayList<XmlItem> items) {
        for(XmlItem item : items)  {
            if(item.getItemType() == XmlItem.Type.STRING) {
                String stringValue = item.getStringValue();
                if(!isEmpty(stringValue))
                    return stringValue;
            }
        }
        return null;
    }

    public static boolean isEmpty(String stringValue) {
        return stringValue==null && stringValue.length()==0;
    }

    public enum OsType
    {
        WINDOWS,
        MAC,
        LINUX,
        OTHER
    }

    public enum FileType
    {
        CALC,
        XML,
        OTHER
    }

    static boolean isExpectedFileExtension(String targetFile,
                                           String[] targetExtensions) {
        String[] temp = targetFile.split(FILE_EXTENSION_DOT);
        if (temp.length < 2) {
            return false;
        }
        String extension = temp[temp.length - 1];
        for (String s : targetExtensions) {
            if (extension.equals(s)) {
                return true;
            }
        }
        return false;
    }

    /***
     * si on choisi de créer un fichier xl, il faut vérifier si le nom du
     * fichier a traiter a bien une extension .XML
     *
     * @return
     * @param option
     * @param filePath
     */
    private static boolean verifyFileExtension(int option, String filePath) {
        if(option == MainClass.Options.OPTION_XL.ordinal())
            return isExpectedFileExtension(filePath, XML_EXTENSIONS);
        else if (option == MainClass.Options.OPTION_XML.ordinal())
            return isExpectedFileExtension(filePath, XL_EXTENSIONS);
        return false;
    }

    public static OsType getOsType(){
        String osType = System.getProperty("os.name");
        System.out.println(osType);
        if(osType.equals(null)) {
            return OsType.OTHER;
        } else if(osStringContains(osType, "win")) {
            return OsType.WINDOWS;
        } else if(osStringContains(osType, "mac")) {
            return OsType.MAC;
        } else if(osStringContains(osType, "nux")) {
            return OsType.LINUX;
        }
        return OsType.OTHER;
    }

    private static boolean osStringContains(String osType, String osString) {
        return osType.indexOf(osString) >= 0;
    }
}
