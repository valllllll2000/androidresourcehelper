package com.vaxapp.android.ressources.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import jxl.Cell;
import jxl.CellType;
import jxl.CellView;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.UnderlineStyle;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/***
 * Classe permettant la manipulation du document excel
 *
 */
public class ExcelReadWrite {
    private String filename;
    private WritableCellFormat formatTitre;
    private WritableCellFormat formatTexte;
    private static final int WRITE_RESULT_OK = 0;
    private static final int WRITE_RESULT_KO = -1;
    private static final String NOM_FEUILLE = "Traductions";

    public String getFileName() {
        return filename;
    }



    public enum RowType {
        STRING, ARRAY_FIRST, ARRAY_NEXT, SKIP
    }

    public ExcelReadWrite(String filename) {
        this.filename = filename;
    }

    public SheetNameAndItems readExcel(int sheetIndex) {
        ArrayList<XmlItem> elements = new ArrayList<XmlItem>();

        File inputWorkbook = new File(filename);
        Workbook w;
        try {
            w = Workbook.getWorkbook(inputWorkbook);

            Sheet sheet = w.getSheet(sheetIndex);
            String sheetName = sheet.getName();
            int left = 0;
            // int first = 1;
            ArrayList<String> arrays = new ArrayList<String>();
            XmlItem item = new XmlItem();
            // colonne 1 strings
            // colonne 2 array items
            for (int i = 0; i < sheet.getRows(); i++) {
                Cell nom = sheet.getCell(left, i);
                Cell next = sheet.getCell(left + 1, i);
                Cell next2 = sheet.getCell(left + 2, i);
                RowType rowtype = getColumnType(nom, next, next2);

                if (rowtype == RowType.STRING) {
                    item = new XmlItem();
                    item.setItemType(XmlItem.Type.STRING);
                    item.setName(nom.getContents());
                    item.setStringValue(next.getContents());

                } else if (rowtype == RowType.ARRAY_FIRST) {
                    item = new XmlItem();
                    item.setItemType(XmlItem.Type.ARRAY);
                    item.setName(nom.getContents());
                    arrays = new ArrayList<String>();
                    arrays.add(next2.getContents());

                } else if (rowtype == RowType.ARRAY_NEXT) {
                    arrays.add(next2.getContents());
                    if (isLastArrayItem(sheet, left, i + 1)) {
                        item.setArrayValue(arrays);
                        elements.add(item);
                    }
                }
            }
            return new SheetNameAndItems(sheetName, elements);
        } catch (BiffException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getNumberOfSheets() {
        File inputWorkbook = new File(filename);
        Workbook w;
        try {
            w = Workbook.getWorkbook(inputWorkbook);
            return w.getNumberOfSheets();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private boolean isLastArrayItem(Sheet sheet, int left, int i) {
        if (i >= sheet.getRows())
            return true;
        Cell next = sheet.getCell(left, i);

        return (next.getType() == CellType.EMPTY) ? true : false;

    }

    private int getWrittenColumns(Workbook w, int index) {
        try {
            Sheet sheet = w.getSheet(index);
            return sheet.getColumns();
        } catch (Exception e) {
            return 0;
        }
    }

    private RowType getColumnType(Cell nom, Cell next, Cell next2) {
        if (nom.getType() != CellType.EMPTY && next.getType() != CellType.EMPTY) {
            return RowType.STRING;
        } else if (nom.getType() == CellType.EMPTY
                && next.getType() == CellType.EMPTY
                && next2.getType() != CellType.EMPTY) {
            return RowType.ARRAY_NEXT;
        } else if (nom.getType() != CellType.EMPTY
                && next.getType() == CellType.EMPTY) {
            return RowType.ARRAY_FIRST;
        }
        return RowType.SKIP;
    }

    /***
     * Extract languages for now only first column
     *
     * @param sheet
     * @return
     */
    private ArrayList<String> extractLanguages(Sheet sheet) {
        ArrayList<String> languages = new ArrayList<String>();
        String lang;
        int j = 1;
        Cell titre = sheet.getCell(j, 0);
        if (titre.getType() == CellType.LABEL) {
            lang = titre.getContents();
            languages.add(lang);
        }
        return languages;
    }

    public int writeExcel(ArrayList<XmlItem> elements, String sheetName, int indexSheet) {
        File file = new File(filename);
        WritableWorkbook book = null;
        Workbook oldbook;
        int numberOfWrittenColumns=0;
        Tools.logMessage("Writing sheet : "+sheetName+", index : "+indexSheet);
        try {
            try {
                oldbook = Workbook.getWorkbook(file);
                numberOfWrittenColumns = getWrittenColumns(oldbook, indexSheet);
                book = Workbook.createWorkbook(file, oldbook);
            } catch (Exception e) {
                book = Workbook.createWorkbook(file, new WorkbookSettings());
            }
            Tools.logMessage(numberOfWrittenColumns+"");

            if(numberOfWrittenColumns==0){
                book.createSheet(sheetName, indexSheet);
            }
            WritableSheet feuille = book.getSheet(indexSheet);

            creerContenu(feuille, elements, numberOfWrittenColumns);

            book.write();
            book.close();
            return WRITE_RESULT_OK;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }

        return WRITE_RESULT_KO;
    }

    private void creerContenu(WritableSheet sheet, ArrayList<XmlItem> elements, int total)
            throws WriteException {
        WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
        formatTexte = new WritableCellFormat(times10pt);
        formatTexte.setWrap(true);

        WritableFont times10ptBoldUnderline = new WritableFont(
                WritableFont.TIMES, 10, WritableFont.BOLD, false,
                UnderlineStyle.SINGLE);
        formatTitre = new WritableCellFormat(times10ptBoldUnderline);
        formatTitre.setWrap(true);

        CellView cv = new CellView();
        cv.setFormat(formatTexte);
        cv.setFormat(formatTitre);
        cv.setAutosize(true);

        int column1=total+1;
        int column2=total+2;
        if(total!=0){
            column1--;
            column2--;
        }

        ajouterTexte(sheet, column1, 0, "Strings", formatTitre);
        ajouterTexte(sheet, column2, 0, "Arrays", formatTitre);

        int ligne = 1;

        for (XmlItem item : elements) {
            if(total==0) ajouterTexte(sheet, 0, ligne, item.getName(), formatTexte);
            if (item.getItemType() == XmlItem.Type.STRING) {
                ajouterTexte(sheet, column1, ligne, item.getStringValue(),
                        formatTexte);
            } else {

                ArrayList<String> values = item.getArrayValue();
                for (String value : values) {
                    ajouterTexte(sheet, column2, ligne, value, formatTexte);
                    ligne++;
                }
            }
            ligne++;
        }
        Tools.logMessage(ligne + " lines written!");

    }

    private void creerContenuMultiple(WritableSheet sheet,
                                      ArrayList<XmlItem> elements)
            throws WriteException {
        WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
        formatTexte = new WritableCellFormat(times10pt);
        formatTexte.setWrap(true);

        WritableFont times10ptBoldUnderline = new WritableFont(
                WritableFont.TIMES, 10, WritableFont.BOLD, false,
                UnderlineStyle.SINGLE);
        formatTitre = new WritableCellFormat(times10ptBoldUnderline);
        formatTitre.setWrap(true);

        CellView cv = new CellView();
        cv.setFormat(formatTexte);
        cv.setFormat(formatTitre);
        cv.setAutosize(true);

        Tools.logMessage("number of columns="+sheet.getColumns());

        ajouterTexte(sheet, 1, 0, "Strings", formatTitre);
        ajouterTexte(sheet, 2, 0, "Arrays", formatTitre);
        int ligne = 1;

        for (XmlItem item : elements) {
            ajouterTexte(sheet, 0, ligne, item.getName(), formatTexte);
            if (item.getItemType() == XmlItem.Type.STRING) {
                ajouterTexte(sheet, 1, ligne, item.getStringValue(),
                        formatTexte);
            } else {

                ArrayList<String> values = item.getArrayValue();
                for (String value : values) {
                    ajouterTexte(sheet, 2, ligne, value, formatTexte);
                    ligne++;
                }
            }
            ligne++;
        }
        Tools.logMessage(ligne + " lines written!");

    }

    private void ajouterTexte(WritableSheet sheet, int colonne, int ligne,
                              String contenu, WritableCellFormat format)
            throws RowsExceededException, WriteException {
        Label label;
        label = new Label(colonne, ligne, contenu, format);
        sheet.addCell(label);
    }

    /***
     * How many different language translations have been provided useful to
     * know how many values/strings.xml files have to be created
     *
     * @param sheet
     * @return
     */
    public int getNumberOfLanguages(Sheet sheet) {
        int j = 0;
        for (int i = 0; i < sheet.getRows(); i++) {
            Cell titre = sheet.getCell(i, 0);
            if (titre.getType() == CellType.LABEL) {
                j++;
            }
        }
        return j;

    }

    public ArrayList<String> getLanguages() {
        ArrayList<String> langs = new ArrayList<String>();
        return langs;
    }

}
