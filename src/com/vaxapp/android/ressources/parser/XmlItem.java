package com.vaxapp.android.ressources.parser;

import java.util.ArrayList;

public class XmlItem {

    public XmlItem() {
    }

    private String name;
    private Type itemType;

    // si element de type string
    private String stringValue;

    // si element de type array
    private ArrayList<String> arrayValue;
    private String lang;

    public XmlItem(String name, ArrayList<String> arrayValue, String lang) {
        this.name = name;
        this.itemType = Type.ARRAY;
        this.arrayValue = arrayValue;
        this.lang = lang;
    }

    public XmlItem(String name, String stringValue, String lang) {
        this.name = name;
        this.itemType = Type.STRING;
        this.stringValue = stringValue;
        this.lang = lang;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getItemType() {
        return itemType;
    }

    public void setItemType(Type itemType) {
        this.itemType = itemType;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public ArrayList<String> getArrayValue() {
        return arrayValue;
    }

    public void setArrayValue(ArrayList<String> arrayValue) {
        this.arrayValue = arrayValue;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    // type d'element
    public enum Type {
        STRING, ARRAY
    }

}
