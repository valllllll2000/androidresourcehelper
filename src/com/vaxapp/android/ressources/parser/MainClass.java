package com.vaxapp.android.ressources.parser;

import javax.swing.*;
import java.awt.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/***
 * Classe principale permettant l'execution du
 *         programme
 */
public class MainClass {

    public static final String ANDROID_RESOURCE_HELPER = "Android Resource Helper";
    private static String filePath;
    private static int option;

    /***set to true to disable gui app ***/
    private static final boolean COMMAND_LINE_ONLY_APP = false;


    public static enum Options {
        OPTION_XL,
        OPTION_XML,
        OPTION_XL_MULTIPLE
    }

    public static void main(String[] args) {
        if(COMMAND_LINE_ONLY_APP)
            initiateCommandLineApp();
        else
            initiateGuiSwingApp();
    }

    private static void initiateGuiSwingApp() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createApplicationScreen();
            }
        });
    }

    private static void createApplicationScreen() {
        JFrame appFrame = new JFrame(ANDROID_RESOURCE_HELPER);
        appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension dimension = new Dimension(500, 300);
        appFrame.setSize(dimension);
        AppContentPane contentPane = new AppContentPane();
        contentPane.setPreferredSize(dimension);
        contentPane.setOpaque(true);
        contentPane.setSize(dimension);
        appFrame.setContentPane(contentPane);

        appFrame.pack();
        appFrame.setVisible(true);
    }

    private static void initiateCommandLineApp() {
        Tools.logMessage("Tapez 0 pour créer le fichier Excel ou \n "
                + "1 pour créer le(s) fichier(s) XML \n"
                + "2 pour créer un fichier Excel à partir d'un dossier 'res' " +
                " de ressources \n");

        option = readOption();
        verifyChosenOption();

        Tools.logMessage("Tapez le chemin et le nom du fichier source");
        filePath = FileHelper.readFileName();
        if(filePath==null && option == Options.OPTION_XL_MULTIPLE.ordinal())
            filePath = "res";
        Tools.logMessage("Le nom du fichier ou dossier est " + filePath);
        if (option == Options.OPTION_XL.ordinal()) {
            FileHelper.createXL(FileHelper.getDefaultXlFileName(), filePath);
        } else if (option == Options.OPTION_XL_MULTIPLE.ordinal()) {
            FileHelper.createXLFromFolder("res");
        } else {
            FileHelper.createXML(filePath);
        }
    }

    private static void verifyChosenOption() {
        if(option == Options.OPTION_XL.ordinal() || option == Options.OPTION_XL_MULTIPLE.ordinal())
                Tools.logMessage("Vous avez choisi de créer un fichier Excel");
        else if(option == Options.OPTION_XML.ordinal())
            Tools.logMessage("Vous avez choisi de créer un fichier XML");
        else informExit("Option invalide");
    }

    /***
     * Imprime message d'erreur et finalise le programme
     * @param message
     */
    protected static void informExit(String message) {
        Tools.logErrorMessage(message);
        System.exit(1);
    }

    /***
     * Lit l'option choisie par l'utilisateur
     * @return option
     */
    private static int readOption() {
        int nbaLire = -1;
        try {
            Scanner scanner = new Scanner(System.in);
            nbaLire = scanner.nextInt();
        } catch (InputMismatchException e) {
            e.printStackTrace();
        }
        return nbaLire;
    }

}
