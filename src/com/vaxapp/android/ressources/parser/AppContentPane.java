package com.vaxapp.android.ressources.parser;

import com.vaxapp.android.ressources.parser.file_chooser.*;
import com.vaxapp.android.ressources.parser.translation.BingTranslator;
import com.vaxapp.android.ressources.parser.translation.TranslationWorker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: valeria
 * Date: 9/14/13
 * Time: 2:49 PM
 */
public class AppContentPane extends JPanel implements ActionListener {

    public static final String INIT_TEXT = "Export XML folder";
    public static final String ACTION_FROM_RES_FOLDER = "0";
    public static final String ACTION_FROM_XL_FILE = "1";
    private static final String INIT_TEXT2 = "Export from XL file";
    public static final String DEFAULT_FILE_NAME = "test_folder/res/values/strings.xml";
    public static final String TIP_0 = "Click here to choose a resource folder and export all its strings to an excel file";
    public static final String TIP_1 = "Click here to choose an Excel file from which to export all strings to Android strings";
    private static final String INIT_TEXT3 = "Exemple 2";
    private static final String ACTION_2 = "2";
    private static final String TIP_2 = "Cliquez ici pour un exemple plus complet";
    private static final String DEFAULT_FOLDER_NAME = "res";
    private static final String INIT_TEXT4 = "Open file";
    private static final String ACTION_OPEN_FILE = "3";
    private static final String TIP_3 = "Click here to open a file";
    private static final String INIT_TEXT5 = "Automatic Translation";
    private static final String ACTION_TRANSLATE_XML_FILE_AND_WRITE_XL = "4";
    private static final String TIP_4 = "Multilingual automatic translation (beta)";

    private final JButton option0Button2;
    private final JButton option0Button4;
    private final JButton option0Button5;
    private final JButton option0Button;
    private BingTranslator bingTranslator;
    private final JProgressBar translateProgressBar;
    private TranslationWorker translationWorker;

    public AppContentPane() {
        Dimension dimension = new Dimension(300, 40);
        Insets insets = new Insets(10, 20, 10, 20);

        option0Button = new JButton(INIT_TEXT);
        option0Button.setActionCommand(ACTION_FROM_RES_FOLDER);
        option0Button.addActionListener(this);
        option0Button.setMnemonic(KeyEvent.VK_0);
        option0Button.setMargin(insets);
        option0Button.setPreferredSize(dimension);
        option0Button.setToolTipText(TIP_0);
        add(option0Button);

        option0Button2 = new JButton(INIT_TEXT2);
        option0Button2.setActionCommand(ACTION_FROM_XL_FILE);
        option0Button2.addActionListener(this);
        option0Button2.setMnemonic(KeyEvent.VK_1);
        option0Button2.setMargin(insets);
        option0Button2.setPreferredSize(dimension);
        option0Button2.setVerticalTextPosition(AbstractButton.BOTTOM);
        option0Button2.setHorizontalTextPosition(AbstractButton.CENTER);
        option0Button2.setToolTipText(TIP_1);
        add(option0Button2);

        option0Button4 = new JButton(INIT_TEXT4);
        option0Button4.setActionCommand(ACTION_OPEN_FILE);
        option0Button4.addActionListener(this);
        option0Button4.setMnemonic(KeyEvent.VK_O);
        option0Button4.setMargin(insets);
        option0Button4.setPreferredSize(dimension);
        option0Button4.setToolTipText(TIP_3);
        add(option0Button4);

        option0Button5 = new JButton(INIT_TEXT5);
        option0Button5.setActionCommand(ACTION_TRANSLATE_XML_FILE_AND_WRITE_XL);
        option0Button5.addActionListener(this);
        option0Button5.setMnemonic(KeyEvent.VK_T);
        option0Button5.setMargin(insets);
        option0Button5.setPreferredSize(dimension);
        option0Button5.setToolTipText(TIP_4);
        add(option0Button5);

        translateProgressBar = new JProgressBar();
        translateProgressBar.setStringPainted(true);
        translateProgressBar.setVisible(false);
        translateProgressBar.setPreferredSize(new Dimension(300, 20));
        add(translateProgressBar);
    }

    private void cancelWorker() {
        translationWorker.cancel(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(ACTION_FROM_RES_FOLDER)) {
            JFileChooser chooser = AbstractFileChooser.createFileChooser(FolderChooser.class);
            new FileChooserAction(chooser, getParent()) {
                @Override
                protected void onFileChosenOk() {
                    FileHelper.createXLFromFolder(getChooserPath());
                }
            };
        } else if (e.getActionCommand().equals(ACTION_FROM_XL_FILE)) {
            JFileChooser chooser = AbstractFileChooser.createFileChooser(XlFileChooser.class);
            new FileChooserAction(chooser, getParent()) {
                @Override
                protected void onFileChosenOk() {
                    FileHelper.createXML(getChooserPath());
                }
            };
        } else if (e.getActionCommand().equals(ACTION_OPEN_FILE)) {
            JFileChooser chooser = AbstractFileChooser.createFileChooser(XlAndXmlFileChooser.class);
            new FileChooserAction(chooser, getParent()) {
                @Override
                protected void onFileChosenOk() {
                    FileHelper.openFile(getChooserPath());
                }
            };
        } else if (e.getActionCommand().equals(ACTION_TRANSLATE_XML_FILE_AND_WRITE_XL)) {
            if (translationWorker == null) {
                JFileChooser chooser = AbstractFileChooser.createFileChooser(XmlFileChooser.class);
                new FileChooserAction(chooser, getParent()) {
                    @Override
                    protected void onFileChosenOk() {
                        translate(getChooserPath());
                    }
                };
            } else {
                cancelWorker();
            }
        }
    }

    private void translate(String chooserPath) {
//        getBingTranslator().extractAndTranslateDefaultFile(chooserPath);
        BingTranslator bingTranslator1 = getBingTranslator();
        if(bingTranslator1 == null){
            //TODO: show error dialog...
            return;
        }
        translationWorker = new TranslationWorker(bingTranslator1, chooserPath);
        translationWorker.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("progress".equals(propertyName)) {
                    translateProgressBar.setIndeterminate(false);
                    translateProgressBar.setValue((Integer) evt.getNewValue());
                } else if ("state".equals(propertyName)) {
                    switch ((SwingWorker.StateValue) evt.getNewValue()) {
                        case DONE:
                            translateProgressBar.setVisible(false);
                            option0Button5.setText(INIT_TEXT5);
                            translationWorker = null;
                            break;
                        case STARTED:
                        case PENDING:
                            option0Button5.setText("Cancel");
                            translateProgressBar.setVisible(true);
                            translateProgressBar.setIndeterminate(true);
                            break;
                    }
                }
            }
        });
        translationWorker.execute();
    }


    public BingTranslator getBingTranslator() {
        if(bingTranslator == null){
            try {
                bingTranslator = new BingTranslator();
            } catch (IOException e) {
                Tools.logErrorMessage("Unable to create a bing translator, make sure you updated the config" +
                        ".properties file with your clientId and clientSecret");
                e.printStackTrace();
                return null;
            }
        }
        return bingTranslator;
    }

}

