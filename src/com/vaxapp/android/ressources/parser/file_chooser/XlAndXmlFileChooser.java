package com.vaxapp.android.ressources.parser.file_chooser;

import javafx.stage.*;

/**
 * Created by valeria on 10/11/14.
 */
public class XlAndXmlFileChooser extends FileChooser {

    XlAndXmlFileChooser() {
        super(FileChooser.FILE_EXTENSION_DESCRIPTION_XL_AND_XML,
                FileChooser.ACCEPTED_EXTENSIONS_XL_AND_XML);
    }
}
