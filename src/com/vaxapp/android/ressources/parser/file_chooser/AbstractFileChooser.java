package com.vaxapp.android.ressources.parser.file_chooser;

import javax.swing.*;

/**
 * Created by valeria on 10/9/14.
 */
public abstract class AbstractFileChooser extends JFileChooser {

    public static final String FILE_CHOOSER_READ_ONLY = "FileChooser.readOnly";

    AbstractFileChooser() {
        super(".");
        setAcceptAllFileFilterUsed(false);
    }

    public static AbstractFileChooser createFileChooser(Class<? extends AbstractFileChooser> folderChooserClass) {
        Boolean old = UIManager.getBoolean(FILE_CHOOSER_READ_ONLY);
        UIManager.put(FILE_CHOOSER_READ_ONLY, Boolean.TRUE);
        AbstractFileChooser chooser = null;
        if (folderChooserClass.equals(FolderChooser.class)) {
            chooser = new FolderChooser();
        } else if(folderChooserClass.equals(XlAndXmlFileChooser.class)){
            chooser = new XlAndXmlFileChooser();
        } else if (folderChooserClass.equals(XlFileChooser.class)){
            chooser = new XlFileChooser();
        } else if (folderChooserClass.equals(XmlFileChooser.class)){
            chooser = new XmlFileChooser();
        }
        UIManager.put(FILE_CHOOSER_READ_ONLY, old);
        return chooser;
    }
}
