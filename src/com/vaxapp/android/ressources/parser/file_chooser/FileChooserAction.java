package com.vaxapp.android.ressources.parser.file_chooser;

import com.vaxapp.android.ressources.parser.Tools;

import javax.swing.*;
import java.awt.*;

/**
* Created by valeria on 10/9/14.
*/
public abstract class FileChooserAction {

    private final JFileChooser chooser;
    private final Container parent;

    protected String getChooserPath() {
        return chooser.getSelectedFile().getAbsolutePath();
    }

    public FileChooserAction(JFileChooser chooser, Container parent1) {
        this.chooser = chooser;
        parent = parent1;
        openDialogAndRetrieveResult();
    }

    private void openDialogAndRetrieveResult(){
        int returnVal = chooser.showOpenDialog(parent);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            onFileChosenOk();
        } else {
            Tools.logMessage("Nothing was chosen");
        }
    }

    protected abstract void onFileChosenOk();

}
