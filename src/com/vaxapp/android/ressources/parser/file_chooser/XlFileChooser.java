package com.vaxapp.android.ressources.parser.file_chooser;

/**
 * Created by valeria on 10/11/14.
 */
public class XlFileChooser extends FileChooser {

    XlFileChooser() {
        super(FileChooser.FILE_EXTENSION_DESCRIPTION_XL,
                FileChooser.ACCEPTED_EXTENSIONS_XL);
    }
}
