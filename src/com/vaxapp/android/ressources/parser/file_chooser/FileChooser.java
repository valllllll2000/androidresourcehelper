package com.vaxapp.android.ressources.parser.file_chooser;

import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Created by valeria on 10/9/14.
 */
public abstract class FileChooser extends AbstractFileChooser {

    public static final String FILE_EXTENSION_DESCRIPTION_XL_AND_XML = ".*xl, *.xls, *.ods, *.xml";
    public static final String FILE_EXTENSION_DESCRIPTION_XL = ".*xl, *.xls, *.ods";
    public static final String[] ACCEPTED_EXTENSIONS_XL_AND_XML = {"xl", "xls", "ods", "xml"};
    public static final String[] ACCEPTED_EXTENSIONS_XL = {"xl", "xls", "ods"};

    FileChooser(String fileExtensionDescription, String[] acceptedExtensions) {
        super();
        FileFilter fileFilter = new FileNameExtensionFilter(fileExtensionDescription, acceptedExtensions);
        setFileFilter(fileFilter);
    }
}
