package com.vaxapp.android.ressources.parser.file_chooser;

/**
 * Created by valeria on 10/11/14.
 */
public class XmlFileChooser extends FileChooser {

    XmlFileChooser() {
        super("*.xml", new String[]{"xml"});
    }
}
