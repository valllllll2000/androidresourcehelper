package com.vaxapp.android.ressources.parser.file_chooser;

import javax.swing.*;

/**
 * Created by valeria on 10/9/14.
 */
public class FolderChooser extends AbstractFileChooser {

    FolderChooser() {
        super();
        setDialogTitle("Select the 'res' folder");
        setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }
}
