package com.vaxapp.android.ressources.parser.translation;

import com.memetix.mst.detect.Detect;
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import com.vaxapp.android.ressources.parser.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: valeria
 * Date: 9/15/13
 * Time: 4:02 PM
 */
public class BingTranslator {

    public BingTranslator() throws IOException {
        PropertiesHandler handler = new PropertiesHandler();
        PropertiesHandler.TranslatorProperties translatorProperties = handler.retrieveProperties();
        Translate.setClientId(translatorProperties.clientId);
        Translate.setClientSecret(translatorProperties.clientSecret);
    }

}
