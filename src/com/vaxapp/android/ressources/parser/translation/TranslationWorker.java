package com.vaxapp.android.ressources.parser.translation;

import com.memetix.mst.detect.Detect;
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import com.vaxapp.android.ressources.parser.*;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by valeria on 10/10/14.
 */
public class TranslationWorker extends SwingWorker<Integer, Void> {

    public Language [] targuetLanguages = {
//            Language.ENGLISH,
            Language.FRENCH,
//            Language.SPANISH,
//            Language.GERMAN,
//            Language.RUSSIAN,
//            Language.CATALAN,
//            Language.PORTUGUESE
    };
    private final BingTranslator bingTranslator;
    private final String originXMLFileName;

    public TranslationWorker(BingTranslator bingTranslator, String originXMLFileName) {
        this.bingTranslator = bingTranslator;
        this.originXMLFileName = originXMLFileName;
    }

    private static void failIfInterrupted() throws InterruptedException {
        if (Thread.currentThread().isInterrupted()) {
            throw new InterruptedException("Interrupted while translating");
        }
    }

    private ArrayList<XmlItem> getTranslatedXmlItems(ArrayList<XmlItem> items, Language languageTo,
                                                     Language languageFrom) {
        ArrayList<XmlItem> newItems = new ArrayList<XmlItem>();
        for(XmlItem item : items) {
            XmlItem newItem = new XmlItem();
            XmlItem.Type itemType = item.getItemType();
            newItem.setItemType(itemType);
            newItem.setLang(languageTo.toString());
            newItem.setName(item.getName());
            if(itemType == XmlItem.Type.STRING) {
                String stringValue = item.getStringValue();
                String translation =
                        !Tools.isEmpty(stringValue)?
                                getTranslation(stringValue, languageFrom, languageTo):
                                null;
                newItem.setStringValue(translation);
            } else if(itemType == XmlItem.Type.ARRAY) {
                ArrayList<String> toTranslate = item.getArrayValue();
                ArrayList<String> translatedValue = getTranslation(toTranslate, languageFrom, languageTo);
                newItem.setArrayValue(translatedValue);
            }
            newItems.add(newItem);
        }
        return newItems;
    }

    private String getTranslation(String stringToTranslate, Language from, Language to) {
        String translatedText = null;
        try {
            translatedText = Translate.execute(stringToTranslate, from, to);
        } catch (Exception e) {
            logTranslationError(e);
        }
        return translatedText;
    }

    private ArrayList<String> getTranslation(ArrayList<String> toTranslate, Language languageFrom, Language languageTo) {
//        String [] arrayOfTranslations = getArrayFromArrayList(toTranslate);
//        String[] translations = getTranslation(arrayOfTranslations, languageFrom, languageTo);
        ArrayList<String>translations =  new ArrayList<String>();
        for (String s: toTranslate){
            translations.add(getTranslation(s, languageFrom, languageTo));
        }
        return translations;
    }

    private ArrayList<String> getArrayListFromArray(String[] translations) {
        ArrayList<String> translationArrayList = new ArrayList<String>();
        for(String s: translations){
            translationArrayList.add(s);
        }
        return translationArrayList;
    }

    private String[] getArrayFromArrayList(ArrayList<String> toTranslate) {
        int size = toTranslate.size();
        String [] array = new String[size];
        for(int i = 0; i < size; i++) {
            array[i] = toTranslate.get(i);
        }
        return array;
    }

    private static String[] getTranslation(String [] stringToTranslate, Language from, Language to) {
        String translatedText [] = new String[0];
        try {
            translatedText = Translate.execute(stringToTranslate, from, to);
            return translatedText;
        } catch (Exception e) {
            Tools.logErrorMessage("Translation error : translatedText : "+translatedText+ "texts : "+stringToTranslate);
            logTranslationError(e);
            return null;
        }
    }

    private static void logTranslationError(Exception e) {
        Tools.logErrorMessage("Translation error");
        e.printStackTrace();
    }

    private Language detectLanguage(String text){
       try {
           Language detectedLanguage = Detect.execute(text);
           Tools.logMessage(Language.FRENCH.toString());
           return detectedLanguage;
       } catch (Exception e) {
           Tools.logErrorMessage("Translation error : text is : "+text);
           e.printStackTrace();
           return null;
       }
   }

    public boolean isSupportedLanguage(Language origin) {
        for(Language l: targuetLanguages) {
            if(origin.equals(l)) {
                Tools.logMessage("Langue : "+l);
                return true;
            }
        }
        return false;
    }


    @Override
    protected Integer doInBackground() throws Exception {

        failIfInterrupted();
        int size = targuetLanguages.length + 1;
        int i = 0;

        ArrayList<XmlItem> items = XmlReadWrite.getXmlItems(originXMLFileName);
        if(items != null && items.size() > 0) {
            String sample = Tools.getFirstNonEmptyString(items);
            Language origin = Language.ENGLISH;
                    //Language.AUTO_DETECT;
                    //detectLanguage(sample);
            failIfInterrupted();
            if(origin == null){
                origin = Language.ENGLISH;
            }
            Tools.logMessage("Source language : "+origin.name());
//            if(!isSupportedLanguage(origin)) {
//                Tools.logMessage("Source language not accepted");
//            } else {
                ArrayList<SheetNameAndItems> sheetNameAndItemses = new ArrayList<SheetNameAndItems>();

                for(Language languageTo: targuetLanguages){
                    failIfInterrupted();
                    if(!origin.equals(languageTo)) {
                        ArrayList<XmlItem> translatedXmlItems = getTranslatedXmlItems(items, languageTo, origin);
                        String fileName = FileHelper.VALUES_FOLDER+"-"+languageTo.toString();
                        sheetNameAndItemses.add(new SheetNameAndItems(fileName, translatedXmlItems));
                    }
                    setProgress((i + 1) * 100 / size);
                    i++;
                }
                if(sheetNameAndItemses != null && sheetNameAndItemses.size() > 0){
                    FileHelper.writeToExcelFile(sheetNameAndItemses);
                    setProgress((i + 1) * 100 / size);
                }
//            }
        }  else {
            Tools.logMessage("Nothing to translate");
        }
        return null;
    }
}
