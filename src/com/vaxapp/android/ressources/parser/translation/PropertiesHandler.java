package com.vaxapp.android.ressources.parser.translation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by valeria on 10/11/14.
 */
public class PropertiesHandler {

    public TranslatorProperties retrieveProperties() throws IOException {
        Properties prop = new Properties();
        String propFileName = "config.properties";

        FileInputStream inputStream = new FileInputStream(propFileName);
        prop.load(inputStream);
        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }
        return new TranslatorProperties(prop.getProperty("client_id"), prop.getProperty("client_secret"));
    }

    public static final class TranslatorProperties {
        public final String clientId;
        public final String clientSecret;

        public TranslatorProperties(String clientId, String secret) {
            this.clientId = clientId;
            this.clientSecret = secret;
        }
    }

}
